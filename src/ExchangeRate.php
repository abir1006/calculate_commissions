<?php

namespace CalculateCommissions;

/**
 * ExchangeRate.php
 *
 * @category  XYZ
 * @package   XYZ
 * @author    Abdoon Nur <abir023048@gmail.com>
 * @copyright 2020 My Company
 * @license   Licence Name
 * @link      XYZ
 * @see       Link to project website
 */
class ExchangeRate
{

    public string $apiUrl;

    /**
     * ExchangeRate constructor.
     *
     * @param $apiUrl
     */
    public function __construct($apiUrl)
    {
        $this->apiUrl = $apiUrl;
    }

    /**
     * Fetch exchange rate from API or Local json
     *
     * @param $currency
     *
     * @return int
     */
    public function getExchangeRate($currency = 'EUR')
    {
        if ($currency == '') {
            exit("Error: Currency not provided");
        }

        if ($currency == 'EUR') {
            return 0;
        }
        try {
            $rates = json_decode(@file_get_contents($this->apiUrl), true);
            if (!isset($rates['rates'][$currency])) {
                exit("Error: Currency '$currency' not available!");
            }
            return $rates['rates'][$currency];
        } catch (\Exception $e) {
            echo "Error: ", $e->getMessage();
        }
    }


    /**
     * @param $exchangeRate
     * @param $amount
     * @return float|int
     */
    public function exchangeAmount($exchangeRate, $amount)
    {
        return $exchangeRate > 0 ?
            $amount / $exchangeRate : $amount;
    }
}
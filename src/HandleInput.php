<?php

namespace CalculateCommissions;

/**
 * HandleInput.php
 *
 * @category  XYZ
 * @package   XYZ
 * @author    Abdoon Nur <abir023048@gmail.com>
 * @copyright 2020 My Company
 * @license   Licence Name
 * @link      XYZ
 * @see       Link to project website
 */

class HandleInput
{
    public $inputDoc;

    /**
     * HandleInput constructor.
     * @param InputInterface $inputDoc
     */
    public function __construct(InputInterface $inputDoc)
    {
        $this->inputDoc = $inputDoc;
    }

    /**
     * @return array
     */
    public function getTransactionData()
    {
        return $this->inputDoc->getFileContent();
    }
}

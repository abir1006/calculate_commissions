<?php

namespace CalculateCommissions;

/**
 * CalculateCommissions.php
 *
 * @category  XYZ
 * @package   XYZ
 * @author    Abdoon Nur <abir023048@gmail.com>
 * @copyright 2020 My Company
 * @license   Licence Name
 * @link      XYZ
 * @see       Link to project website
 */
class CalculateCommissions
{
    public $isEu;
    public $euroCommission = 0.01;
    public $nonEuroCommission = 0.02;


    /**
     * @param $exchangeAmount
     * @param $countryCode
     * @return false|float
     */
    public function commissionAmount($exchangeAmount, $countryCode)
    {
        if ($exchangeAmount == '' or $countryCode == '') {
            exit('Error: exchange amount and country code not provided');
        }
        $this->isEu = CountryCode::isEu($countryCode);
        $commission = ($this->isEu === true) ?
            $this->euroCommission : $this->nonEuroCommission;
        return round($exchangeAmount * $commission, 2);
    }
}

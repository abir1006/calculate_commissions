<?php


namespace CalculateCommissions;

/**
 * CountryCode.php
 *
 * @category  XYZ
 * @package   XYZ
 * @author    Abdoon Nur <abir023048@gmail.com>
 * @copyright 2020 My Company
 * @license   Licence Name
 * @link      XYZ
 * @see       Link to project website
 */
class CountryCode
{

    private static $euCountryCodes = array(
        'AT',
        'BE',
        'BG',
        'CY',
        'CZ',
        'DE',
        'DK',
        'EE',
        'EL',
        'ES',
        'FI',
        'FR',
        'GR',
        'HR',
        'HU',
        'IE',
        'IT',
        'LT',
        'LU',
        'LV',
        'MT',
        'NL',
        'PL',
        'PT',
        'RO',
        'SE',
        'SI',
        'SK'
    );

    public string $apiUrl;

    /**
     * CountryCode constructor.
     *
     * @param $apiUrl
     */
    public function __construct($apiUrl)
    {
        $this->apiUrl = $apiUrl;
    }

    /**
     * Check if country is in EU or non EU
     *
     * @param $countryCode Country Code to find if it is EU
     *
     * @return bool
     */
    public static function isEu($countryCode)
    {
        if (empty($countryCode)) {
            exit('Error: Country code not found');
        }
        return in_array($countryCode, self::$euCountryCodes);
    }

    /**
     * Fetch country code from bin value
     *
     * @param string $bin
     *
     * @return string
     */
    public function retrieveCountryCode($bin)
    {
        if ($this->apiUrl == '') {
            exit('Error: API url not provided');
        }

        try {
            $data = json_decode(@file_get_contents($this->apiUrl . '/' . $bin), true);
            if (!$data['country']['alpha2']) {
                exit("Error: Invalid BIN: $bin");
            }
            return $data['country']['alpha2'];
        } catch (\Exception $e) {
            return "Error: " . $e->getMessage();
        }
    }
}
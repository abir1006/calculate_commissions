<?php

namespace CalculateCommissions;

/**
 * InputFromTextFile.php
 *
 * @category  XYZ
 * @package   XYZ
 * @author    Abdoon Nur <abir023048@gmail.com>
 * @copyright 2020 My Company
 * @license   Licence Name
 * @link      XYZ
 * @see       Link to project website
 */
class InputFromTextFile implements InputInterface
{
    private $_path;

    /**
     * InputFromTextFile constructor.
     *
     * @param $path
     */
    public function __construct($path)
    {
        $this->_path = $path;
    }

    /**
     * Shareable abstract method to get fetch transaction content as array
     *
     * @return array
     */
    public function getFileContent(): array
    {
        $file = fopen($this->_path, "r") or exit("Unable to open the file!");
        $data = [];
        if (filesize($this->_path) == 0) {
            return $data;
        }
        while (!feof($file)) {
            $data[] = json_decode(fgets($file), true);
        }
        fclose($file);

        return $data;
    }
}
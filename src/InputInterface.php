<?php

namespace CalculateCommissions;

/**
 * InputInterface.php
 *
 * @category  XYZ
 * @package   XYZ
 * @author    Abdoon Nur <abir023048@gmail.com>
 * @copyright 2020 My Company
 * @license   Licence Name
 * @link      XYZ
 * @see       Link to project website
 */
interface InputInterface
{
    /**
     * Method to force implement all child classes
     *
     * @return array
     */
    public function getFileContent(): array;
}
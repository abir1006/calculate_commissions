Please follow the below steps to run the application: 

1. Clone respository.
2. Change directory to project folder.
3. Run composer install to install your php dependencies.
4. Run php app.php input.txt to see the output
5. Run this command to see unit test: ./vendor/bin/phpunit tests
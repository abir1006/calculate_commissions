<?php

require './vendor/autoload.php';

use CalculateCommissions\InputFromTextFile as InputFile;
use CalculateCommissions\CalculateCommissions;
use CalculateCommissions\CountryCode;
use CalculateCommissions\ExchangeRate;
use CalculateCommissions\HandleInput;

$inputFilePath = (isset($argv[1]) && $argv[1]) ? $argv[1] : 'input.txt';

$handleInput = new HandleInput(new InputFile($inputFilePath));

$transactionData = $handleInput->getTransactionData();

$countyCode = new CountryCode('https://lookup.binlist.net');

$exchangeRate = new ExchangeRate('https://api.exchangeratesapi.io/latest');

$calCommissions = new CalculateCommissions();

// Iterate through transaction data
$commissionAmounts = array_map(
    function ($transaction) use ($countyCode, $exchangeRate, $calCommissions) {

        $countryCode = $countyCode->retrieveCountryCode($transaction['bin']);

        $rate = $exchangeRate->getExchangeRate($transaction['currency']);

        $exchangeAmount = $exchangeRate->exchangeAmount($rate, $transaction['amount']);

        return $calCommissions->commissionAmount($exchangeAmount, $countryCode);
    },
    $transactionData
);

// Render output
foreach ($commissionAmounts as $commission) {
    echo $commission;
    print "\n";
}
<?php

/**
 * Test File
 * PHP version 7.1
 *
 * @category  UnitTest
 * @package   PHPUnit
 * @author    Abdoon Nur <abir023048@gmail.com>
 * @copyright 2020 My Company
 * @license   Licence Name
 * @link      XYZ
 * @see       Link to project website
 */

declare(strict_types=1);

namespace CalculateCommissions;

use PHPUnit\Framework\TestCase;

/**
 * ExchangeRateTest.php
 *
 * @category  XYZ
 * @package   XYZ
 * @author    Abdoon Nur <abir023048@gmail.com>
 * @copyright 2020 My Company
 * @license   Licence Name
 * @link      XYZ
 * @see       Link to project website
 */
class ExchangeRateTest extends TestCase
{
    /**
     * Test method testExchangeRateIsZeroWhenEURO
     *
     * @return void
     */
    public function testExchangeRateIsZeroWhenEURO()
    {
        $exchangeRate = new ExchangeRate();
        $this->assertEquals(0, $exchangeRate::getRate('EUR'));
    }

    /**
     * Test method testMatchFailedExchangeRate
     *
     * @return void
     */
    public function testIfExchangeRateCanFetchFromCurrency()
    {
        $exchangeRate = new ExchangeRate('https://api.exchangeratesapi.io/latest');
        $this->assertEquals(
            0.87498,
            $exchangeRate->getExchangeRate('USD')
        );
    }
}

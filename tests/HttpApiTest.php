<?php

/**
 * Test File
 * PHP version 7.1
 *
 * @category  UnitTest
 * @package   PHPUnit
 * @author    Abdoon Nur <abir023048@gmail.com>
 * @copyright 2020 My Company
 * @license   Licence Name
 * @link      XYZ
 * @see       Link to project website
 */

declare(strict_types=1);

namespace CalculateCommissions;

use InterNations\Component\HttpMock\PHPUnit\HttpMockTrait;
use PHPUnit\Framework\TestCase;

/**
 * HttpApiTest.php
 *
 * @category  XYZ
 * @package   XYZ
 * @author    Abdoon Nur <abir023048@gmail.com>
 * @copyright 2020 My Company
 * @license   Licence Name
 * @link      XYZ
 * @see       Link to project website
 */
class HttpApiTest extends TestCase
{
    use HttpMockTrait;

    /**
     *
     */
    public static function setUpBeforeClass()
    {
        static::setUpHttpMockBeforeClass('9000', 'localhost');
    }

    /**
     *
     */
    public static function tearDownAfterClass()
    {
        static::tearDownHttpMockAfterClass();
    }

    /**
     *
     */
    public function setUp()
    {
        $this->setUpHttpMock();
    }

    /**
     *
     */
    public function tearDown()
    {
        $this->tearDownHttpMock();
    }

    /**
     *
     */
    public function testExchangeRateFakeHttpRequest()
    {
        $this->http->mock
            ->when()
            ->methodIs('GET')
            ->pathIs('/api/latest_exchange_rates')
            ->then()
            ->body('{"rates":{"CAD":1.5179,"HKD":8.4301}}')
            ->end();

        $this->http->setUp();

        $exchangeRate = new ExchangeRate('http://localhost:9000/api/latest_exchange_rates');

        $this->assertSame(1.5179, $exchangeRate->getExchangeRate('CAD'));
    }

    /**
     *
     */
    public function testUSCountryCodeFakeHttpRequest()
    {
        $this->http->mock
            ->when()
            ->methodIs('GET')
            ->pathIs('/api/lockup_bin_list/41417360')
            ->then()
            ->body('{"number": {}, "scheme": "visa", "country": {"numeric": "840", "alpha2": "US"}}')
            ->end();

        $this->http->setUp();

        $countryCodeObj = new CountryCode('http://localhost:9000/api/lockup_bin_list');

        $this->assertSame('US', $countryCodeObj->retrieveCountryCode('41417360'));
    }

    /**
     *
     */
    public function testExceptionHttpRequest()
    {
        $this->http->mock
            ->when()
            ->methodIs('GET')
            ->pathIs('/api/lockup_bin_list/41417360')
            ->then()
            ->body('{"number": {}, "scheme": "visa", "country": {"numeric": "840", "alpha2": "US"}}')
            ->end();

        $this->http->setUp();

        $countryCodeObj = new CountryCode('http://localhost:9000/api/lockup_bin_list_error');

        $this->assertSame('US', $countryCodeObj->retrieveCountryCode('41417360'), 'API URL not valid');
    }
}

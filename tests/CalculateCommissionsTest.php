<?php

/**
 * Test File
 * PHP version 7.1
 *
 * @category  UnitTest
 * @package   PHPUnit
 * @author    Abdoon Nur <abir023048@gmail.com>
 * @copyright 2020 My Company
 * @license   Licence Name
 * @link      XYZ
 * @see       Link to project website
 */

declare(strict_types=1);

namespace CalculateCommissions;

use PHPUnit\Framework\TestCase;

/**
 * CalculateCommissionsTest.php
 *
 * @category  XYZ
 * @package   XYZ
 * @author    Abdoon Nur <abir023048@gmail.com>
 * @copyright 2020 My Company
 * @license   Licence Name
 * @link      XYZ
 * @see       Link to project website
 */
class CalculateCommissionsTest extends TestCase
{
    /**
     * Test method testCommissionAmountIsCalculatedExactly
     *
     * @return void
     */
    public function testCommissionAmountIsCalculatedExactly()
    {
        $commissionCalc = new CalculateCommissions();
        $commissionCalc->euroCommission = 0.01;
        $commissionCalc->nonEuroCommission = 0.02;
        $this->assertEquals(0.46, $commissionCalc->commissionAmount(46.010858562621, 'LT'));
    }

    /**
     * Test method testCommissionAmountIsCalculatedExactly
     *
     * @return void
     */
    public function testFailedIfCommissionRateChanged()
    {
        $commissionCalc = new CalculateCommissions();
        $commissionCalc->euroCommission = 0.03;
        $commissionCalc->nonEuroCommission = 0.09;
        $this->assertEquals(0.46, $commissionCalc->commissionAmount(46.010858562621, 'LT'));
    }

    /**
     * Test method testCommissionAmountSame
     *
     * @return void
     */
    public function testCommissionAmountSameFailed()
    {
        $commissionCalc = new CalculateCommissions();
        $this->assertSame(0.49, $commissionCalc->commissionAmount(46.010858562621, 'LT'));
    }
}

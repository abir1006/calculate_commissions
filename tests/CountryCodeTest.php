<?php

/**
 * Test File
 * PHP version 7.1
 *
 * @category  UnitTest
 * @package   PHPUnit
 * @author    Abdoon Nur <abir023048@gmail.com>
 * @copyright 2020 My Company
 * @license   Licence Name
 * @link      XYZ
 * @see       Link to project website
 */

declare(strict_types=1);

namespace CalculateCommissions;

use PHPUnit\Framework\TestCase;

/**
 * CountryCodeTest.php
 *
 * @category  XYZ
 * @package   XYZ
 * @author    Abdoon Nur <abir023048@gmail.com>
 * @copyright 2020 My Company
 * @license   Licence Name
 * @link      XYZ
 * @see       Link to project website
 */
class CountryCodeTest extends TestCase
{
    /**
     * Test method testExchangeRateIsZeroWhenEURO
     *
     * @return void
     */
    public function testIsEu()
    {
        $countryCodeObj = new CountryCode();
        $this->assertEquals(true, $countryCodeObj::isEu('LT'));
    }

    /**
     * Test method testExchangeRateIsZeroWhenEURO
     *
     * @return void
     */
    public function testIfCountryCodeCanFetchFromBin()
    {
        $countryCodeObj = new CountryCode('https://lookup.binlist.net');
        $this->assertEquals('GB', $countryCodeObj->retrieveCountryCode('4745030'));
    }
}
